import collections

from django.conf import settings
from django.db.models import QuerySet

from bbql.parser import BaseTransformer


def execute(v, query=None, sortcol=None):
    if isinstance(v, QuerySet):
        transformer = BaseTransformer.newinstance(
            getattr(settings, 'BBQL_ORM_TRANSFORMER',
                    'bbql.orm.ORMTransformer'), v.model)
    elif isinstance(v, collections.Iterable):
        transformer = BaseTransformer.newinstance(
            getattr(settings, 'BBQL_ITERABLE_TRANSFORMER',
                    'bbql.iterable.IterableTransformer'))
    else:
        return v

    if query:
        v = transformer.filter(v, query)
    if sortcol:
        v = transformer.order_by(v, sortcol)

    return v
